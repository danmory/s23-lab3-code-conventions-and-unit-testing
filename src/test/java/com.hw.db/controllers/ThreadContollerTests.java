package com.hw.db.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;

import java.sql.Timestamp;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hw.db.controllers.ThreadController;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

public class ThreadContollerTests {
  private Thread thread;
  private Post post;
  private User user;
  private ThreadController controller;

  @BeforeEach
  void init() {
    controller = new ThreadController();
    Timestamp ts = new Timestamp(0);
    post = new Post("author", ts, "forum", "message", 0, 1, false);
    user = new User("nickname", "email@email.ru", "fullname", "about");
    thread = new Thread("author", ts, "forum", "message", "slug", "title", 0);
    thread.setId(0);
  }

  @Test
  @DisplayName("Check ID or Slug")
  void testCheckIdOrSlug() {
    try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
      threadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
      assertEquals(thread, controller.CheckIdOrSlug("0"));
      threadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
      assertEquals(thread, controller.CheckIdOrSlug("slug"));
    }
  }

  @Test
  @DisplayName("Create post")
  void testCreatePost() {
    try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
      try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
        threadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
        userDAO.when(() -> UserDAO.Info("author")).thenReturn(user);
        ResponseEntity res = controller.createPost("0", List.of(post));
        assertEquals(HttpStatus.CREATED, res.getStatusCode());
      }
    }
  }

  @Test
  @DisplayName("Posts")
  void testPosts() {
    try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
      threadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
      threadDAO.when(() -> ThreadDAO.getPosts(thread.getId(), 1, 0, "sort", false)).thenReturn(List.of(post));
      ResponseEntity res = controller.Posts("slug", 1, 0, "sort", false);
      assertEquals(HttpStatus.OK, res.getStatusCode());
    }
  }

  @Test
  @DisplayName("Change")
  void testChange() {
    try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
      threadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
      ResponseEntity res = controller.change("slug", thread);
      assertEquals(HttpStatus.OK, res.getStatusCode());
    }
  }

  @Test
  @DisplayName("Info")
  void testInfo() {
    try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
      threadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
      ResponseEntity res = controller.info("slug");
      assertEquals(HttpStatus.OK, res.getStatusCode());
      assertEquals(thread, res.getBody());
    }
  }

  @Test
  @DisplayName("Create vote")
  void testCreateVote() {
    try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
      try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
        threadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
        userDAO.when(() -> UserDAO.Info("nickname")).thenReturn(user);
        ResponseEntity res = controller.createVote("slug", new Vote("nickname", 1));
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertEquals(thread, res.getBody());
      }
    }
  }
}
